# jelk

**J**enkins + **E**lasticsearch + **L**ogstash + **K**ibana

Uses the [jenkins/jenkins:lts](https://hub.docker.com/r/jenkins/jenkins/) container and [deviantony/docker-elk](https://github.com/deviantony/docker-elk) Docker Compose stack.

## Run

````
dockercompose up -d
````

## Info

* Jenkins: http://localhost:8080
* Elasticsearch: http://localhost:9200
* Logstash: http://localhost:5000
* Kibana: http://localhost:5601

Jenkins will require the installer to be completed, to obtain the admin password, find the container id for Jenkins and run:

````
docker exec -t <container-id> cat /var/jenkins_home/secrets/initialAdminPassword
````
