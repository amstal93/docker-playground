# traefik-docker

Simple [Traefik](https://traefik.io/) implementation with [Docker backend](https://docs.traefik.io/configuration/backends/docker) config.

## About

Runs 1 Traefik and 2 Nginx containers.

The Traefik frontend routes to one of the Nginx containers based on the host (or itself), this to demonstrate the basic config with a Docker backend and to tinker with basic Traefik concepts.

## Setup

Add the following to you `/etc/hosts`:

````
127.0.0.1 	docker.localhost *.docker.localhost
````

Start the containers using `docker-compose`:

````
docker-compose up -d
````

## Usage

* Traefik UI: http://docker.localhost:8080
* Nginx 1: http://web-01.docker.localhost
* Nginx 2: http://web-02.docker.localhost
