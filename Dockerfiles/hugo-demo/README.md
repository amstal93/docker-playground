# hugo-demo

Demo setup for [docker-hugo](https://gitlab.com/sjugge/docker-hugo)

## Build and run

````
docker build -t hugo-demo .  
docker run -dit -p 1313:1313 --mount type=bind,source=`pwd`/app,destination=/app hugo-demo
````
