# zsh-fedora

Run zsh setup in Fedora container.

## Build

````
docker build -t zsh-fedora .
````

## Run

````
docker run -dit --name zsh-fedora-$(date +%Y-%m-%d--%H-%M) zsh-fedora /bin/zsh
````

## Exec

````
docker exec -it $(docker ps -q) /bin/zsh
````
