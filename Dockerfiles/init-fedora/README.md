# init-fedora

Get a fresh Fedora install ready to go with package installations.

See [sjugge/init/fedora-workstation](https://github.com/sjugge/init/tree/master/fedora-workstation) for most current version.
While the `Dockerfile` version is convenient for rapid testing, containers are lacking compared to VM's.

## Build

````
docker build -t init-fedora .
````

## Run

````
docker run -d -i -t bootstrap-fedora
````

Run container and script:

````
docker run -i -t bootstrap-fedora ./init.sh
````

## Exec

````
docker exec -i -t $(docker ps -a) /bin/bash
````
